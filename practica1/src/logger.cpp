#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
//crea tuberia
	mkfifo("/tmp/loggerfifo",0777);
//abre en modo lectura
	int fd=open("/tmp/loggerfifo", O_RDONLY);
	int salir=0;
	int aux;
//bucle infinito
	while(salir==0)
	{
		char buff[200];
		aux=read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
		if((buff[0]=='-')||(aux==-1)) 
		{
			printf("Cerramos logger. tenis cerrado \n");
		salir=1; 
		}

	}
//destruir la tuberia
	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}

